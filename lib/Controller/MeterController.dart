import 'package:http/http.dart' as http;
import 'dart:io';
import 'dart:convert';
import '../Utils.dart';

class MeterController {

    dynamic _get(String url) async {
        final response = await http.get(url);

        if (response.statusCode == 200) {
            print(response.body);
            final map = json.decode(response.body);
            return map;
        } else {
            return Null;
        }
    }

    dynamic _post(String url, String body) async {
        final response = await http.post(url, body: body);
        if (response.statusCode == 200) {
            print(response.body);
            final map = json.decode(response.body);
            return map;
        } else {
            return Null;
        }
    }
}