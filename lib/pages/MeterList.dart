// Copyright 2017 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '../Utils.dart';
import 'MeterOps.dart';

class AnimatedListSample extends StatefulWidget {
    List<int> meterIdList;
    String house;
    @override
    _AnimatedListSampleState createState() => new _AnimatedListSampleState(house, meterIdList);
    AnimatedListSample(this.house, this.meterIdList);
}

class _AnimatedListSampleState extends State<AnimatedListSample> {
    final GlobalKey<AnimatedListState> _listKey = new GlobalKey<AnimatedListState>();
    ListModel<MeterDetail> _list;
    MeterDetail _selectedItem;
    String house;
    List<int> meterIdList;
    var _isLoading = false;
    List<MeterDetail> meterDetailList = [];

    _AnimatedListSampleState(this.house, this.meterIdList);

    void _updateListModel(List<MeterDetail> list){
        _list = new ListModel<MeterDetail>(
            listKey: _listKey,
            initialItems: list,
            removedItemBuilder: _buildRemovedItem,
        );
    }

    @override
    void initState() {
        super.initState();
        _updateListModel(meterDetailList);

        setState(() {
            _isLoading = true;
        });
        _fetchMeterInfo();
    }

    _fetchMeterInfo() async {
        meterDetailList = [];
        for(int i = 0; i < meterIdList.length; i++){
            int meterId = meterIdList[i];
            var meter = new MeterDetail(meterId);
            final url = "http://www.asuscn.space:3003/meter/$meterId";
            print("[MeterList]Atempting to get data from " + url);
            final response = await http.get(url);

            if (response.statusCode == 200) {
                print(response.body);
                final map = json.decode(response.body);
                var meterDetail = map.length > 0 ? map[0]: Null;
                if(meterDetail != Null){
                    if (meterDetail["state"].toString() == "204"){
                        meter.switchState = 1;
                    } else if (meterDetail["state"].toString() == "221"){
                        meter.switchState = 0;
                    }
                }
                meter.meterDetail = meterDetail;
            } else {
                meter.switchState = -1;
            }
            meterDetailList.add(meter);
            _updateListModel(meterDetailList);
            //sleep(new Duration(seconds: 1));
        }
        setState(() {
                _isLoading = false;
            });
    }
    
    // Used to build list items that haven't been removed.
    Widget _buildItem(
        BuildContext context, int index, Animation<double> animation) {
        return new CardItem(
        animation: animation,
        item: _list[index],
        selected: _selectedItem == null ? false: _selectedItem.meterId == _list[index].meterId,
        onTap: () {
            setState(() {
                //_selectedItem = _selectedItem == _list[index] ? null : _list[index];
                //if(_selectedItem != null){
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => MeterOps(house, _list[index]),
                    ));
                //}
                
            });
        },
        );
    }

    // Used to build an item after it has been removed from the list. This method is
    // needed because a removed item remains  visible until its animation has
    // completed (even though it's gone as far this ListModel is concerned).
    // The widget will be used by the [AnimatedListState.removeItem] method's
    // [AnimatedListRemovedItemBuilder] parameter.
    Widget _buildRemovedItem(
        MeterDetail item, BuildContext context, Animation<double> animation) {
        return new CardItem(
        animation: animation,
        item: item,
        selected: false,
        // No gesture detector here: we don't want removed items to be interactive.
        );
    }

    // Insert the "next item" into the list model.
    void _insert() {
        final int index = _list.length;
        _list.insert(index, _selectedItem);
    }

    // Remove the selected item from the list model.
    void _remove() {
        if (_selectedItem != null) {
        _list.removeAt(_list.indexOf(_selectedItem));
        setState(() {
            _selectedItem = null;
        });
        }
    }

    // Do more operations on the selected item.
    void _controll() {
        if (_selectedItem != null) {
        //TODO: navigate to controller page
        print("TODO: navigate to controller page");
        }
    }

    @override
    Widget build(BuildContext context) {
        return new MaterialApp(
        home: new Scaffold(
            appBar: new AppBar(
                leading: new IconButton(
                    onPressed: (){
                        Navigator.pop(context);
                    },
                    icon: BackButtonIcon(),
                ),
            title: Text(this.house),

            ),
            body: new Center(
                child : new Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: _isLoading == true ? new CircularProgressIndicator(): new AnimatedList(
                        key: _listKey,
                        initialItemCount: _list.length,
                        itemBuilder: _buildItem,
                    ),
                ),
            ),
            
        ),
        );
    }
}

/// Keeps a Dart List in sync with an AnimatedList.
///
/// The [insert] and [removeAt] methods apply to both the internal list and the
/// animated list that belongs to [listKey].
///
/// This class only exposes as much of the Dart List API as is needed by the
/// sample app. More list methods are easily added, however methods that mutate the
/// list must make the same changes to the animated list in terms of
/// [AnimatedListState.insertItem] and [AnimatedList.removeItem].
class ListModel<E> {
    ListModel({
        @required this.listKey,
        @required this.removedItemBuilder,
        Iterable<E> initialItems,
    })  : assert(listKey != null),
            assert(removedItemBuilder != null),
            _items = new List<E>.from(initialItems ?? <E>[]);

    final GlobalKey<AnimatedListState> listKey;
    final dynamic removedItemBuilder;
    final List<E> _items;

    AnimatedListState get _animatedList => listKey.currentState;

    void insert(int index, E item) {
        _items.insert(index, item);
        _animatedList.insertItem(index);
    }

    E removeAt(int index) {
        final E removedItem = _items.removeAt(index);
        if (removedItem != null) {
        _animatedList.removeItem(index,
            (BuildContext context, Animation<double> animation) {
            return removedItemBuilder(removedItem, context, animation);
        });
        }
        return removedItem;
    }

    int get length => _items.length;
    E operator [](int index) => _items[index];
    int indexOf(E item) => _items.indexOf(item);
}

/// Displays its integer item as 'item N' on a Card whose color is based on
/// the item's value. The text is displayed in bright green if selected is true.
/// This widget's height is based on the animation parameter, it varies
/// from 0 to 128 as the animation varies from 0.0 to 1.0.
class CardItem extends StatelessWidget {
    const CardItem(
        {Key key,
        @required this.animation,
        this.onTap,
        @required this.item,
        this.selected: false})
        : assert(animation != null),
            assert(item != null),
            assert(selected != null),
            super(key: key);

    final Animation<double> animation;
    final VoidCallback onTap;
    final MeterDetail item;
    final bool selected;

    @override
    Widget build(BuildContext context) {
        TextStyle textStyle = Theme.of(context).textTheme.display1;
        if (selected)
            textStyle = textStyle.copyWith(color: Colors.lightGreenAccent[400]);
        //DetailPageState detailPageState = DetailPage.of(context);
        Map meter = item.meterDetail;
        String titleStr = "电表: " + meter["mac"].toString();
        String subtitleStr = "电表示数: " + meter["value"].toString() + "\n" +
            "电表持续更新次数: " + meter["updateTimes"].toString() + "\n" +
            "电表最近更新时间: " + meter["lastUpdateTime"].toString();
        return new Padding(
        padding: const EdgeInsets.all(2.0),
        child: new SizeTransition(
            axis: Axis.vertical,
            sizeFactor: animation,
            child: new GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: onTap,
            child: new SizedBox(
                //height: 300.0,
                child: new Card(
                    color: selected ? Colors.white70: Colors.white,
                    child: new Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                        ListTile(
                            leading: const Icon(Icons.album),
                            title: new Text(titleStr),
                            subtitle: new Text(subtitleStr),
                        ),
                        ],
                    ),
                ),
            ),
            ),
        ),
        );
    }
}
