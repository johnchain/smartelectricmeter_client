import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '../Utils.dart';

class MeterOps extends StatefulWidget{
	MeterDetail meter;
    String house;
    
	@override
	State<StatefulWidget> createState() {
        return new MeterOpsState(this.house, this.meter);
	}
	MeterOps(this.house, this.meter);
}

class MeterOpsState extends State<MeterOps> {
    MeterDetail meter;
    String house;
    var _isLoading = false;
	var _switchState = true;
    int _radioSelection = 1;
    
    MeterOpsState(this.house, this.meter);

    @override
	void initState() {
		super.initState();
        setState(() {
            _isLoading = false;
        });
        //_fetchMeterInfo();
	}

    Column buildButtonColumn(IconData icon, String label) {
        Color color = Theme.of(context).primaryColor;

        return new Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
            new Icon(icon, color: color),
            new Container(
            margin: const EdgeInsets.only(top: 8.0),
            child: new Text(
                label,
                style: new TextStyle(
                fontSize: 12.0,
                fontWeight: FontWeight.w400,
                color: color,
                ),
            ),
            ),
        ],
        );
    }

    @override
    Widget build(BuildContext context) {
        return new MaterialApp(
			home: new Scaffold(
				appBar: new AppBar(
                    leading: new IconButton(
                        onPressed: (){
                            Navigator.pop(context);
                        },
                        icon: BackButtonIcon(),
                    ),
					title: new Text(meter.meterDetail["mac"].toString()),
				),
			body: new Center(
				child: _isLoading ? new CircularProgressIndicator() : 
                new Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                            //Material内置控件
                            new Text('', style: new TextStyle(color: Colors.black,)),
                            new Text('设置', style: new TextStyle(color: Colors.black,)),
                            new Divider(height: 15.0,color: Colors.black,),
                            new Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                    new Text('开关切换：', style: new TextStyle(color: Colors.black,)),
                                    new Switch(value: _switchState, onChanged: (bool value){onSwitchChanged(value);},),
                                ],),
                            new Divider(height: 15.0,color: Colors.grey,),

                            new Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                    new Text('主从切换：', style: new TextStyle(color: Colors.black,)),
                                    new Column(
                                        children: makeRadioList(),
                                    )
                                ],),
                            new Divider(height: 15.0,color: Colors.grey,),

                            new Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                    new FlatButton(
                                        padding: const EdgeInsets.all(8.0),
                                        textColor: Colors.red,
                                        onPressed: deleteMeter,
                                        child: new Text("删除点表"),
                                    ),
                                ],),
                            new Divider(height: 15.0,color: Colors.grey,),
                    ]),
		    ),
		));
    }
    void onSwitchChanged(bool value){
        //bool value = value
        postMeterDetail(meter.meterDetail, PostCMDs.SWITCH, value ? "on": "off");
        // setState(() {
        //     _switchState = value;
        // });
    }

    void deleteMeter(){
        postMeterDetail(meter.meterDetail, PostCMDs.UNBIND, "");
    }

    void onRadioChanged(int value){
        setState(() {
            _radioSelection = value;
            print("TODO: switch status to " + value.toString());
        });
    }
    List<Widget> makeRadioList(){
        List<Widget> list = new List<Widget>();

        list.add(
            new Row(
                children: <Widget>[
                    new Text("主设备", ),
                    new Radio(value: 1, groupValue: _radioSelection, onChanged: (int value){onRadioChanged(value);},),
                ]
            ),
        );
        list.add(
            new Row(
                children: <Widget>[
                    new Text("从设备", ),
                    new Radio(value: 2, groupValue: _radioSelection, onChanged: (int value){onRadioChanged(value);},),
                ]
            ),
        );
        return list;
    }

    _fetchMeterInfo() async {
        int meterId = this.meter.meterId;
        var meter = new MeterDetail(meterId);
        final url = "http://www.asuscn.space:3003/meter/$meterId";
        print("[MeterOps]Atempting to get data from " + url);
        final response = await http.get(url);

        if (response.statusCode == 200) {
            print(response.body);
            final map = json.decode(response.body);
            var meterDetail = map.length > 0 ? map[0]: Null;
            if(meterDetail != Null){
                if (meterDetail["state"].toString() == "204"){
                    meter.switchState = 1;
                } else if (meterDetail["state"].toString() == "221"){
                    meter.switchState = 0;
                }
            }
            meter.meterDetail = meterDetail;
        } else {
            meter.switchState = -1;
        }
        
        setState(() {
            _isLoading = false;
        });
    }

    postMeterDetail(Map pageInfo, PostCMDs cmd, String value) async {
        if(pageInfo == Null){
            print("Pageinfo empty!!!");
            return ;
        }else{
            var mac = pageInfo["mac"].toString();
            const baseURL = "http://www.asuscn.space:3003/";
            switch (cmd) {
            case PostCMDs.SWITCH:
                const url = baseURL + "meter_switch";
                var body = '{"function":"switch", "key":"${mac}", "value":"${value}"}';
                final response = await http.post(url, body: body);
                print("响应状态： ${response.statusCode}");
                print("响应正文： ${response.body}");
                final rstBody = json.decode(response.body);
                if(response.statusCode == 200 && rstBody["status"] == "success"){
                    _switchState = !_switchState;
                }
                break;
            case PostCMDs.UPDATE:
                break;
            case PostCMDs.UNBIND:
                int meterId = this.meter.meterId;
                const url = baseURL + "meter_unbind";
                var body = '{"function":"unbind", "key":"${meterId}", "value":"${value}"}';
                final response = await http.post(url, body: body);
                if(response.statusCode == 200){
                    Navigator.pop(context);
                }else{
                    print("delete failed, [${response.statusCode}] ${response.body}");
                }
                break;
            default:
                break;
            }
        }

        setState(() {
            _isLoading = false;
        });
	}
}

    