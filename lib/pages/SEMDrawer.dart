import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SEMDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Drawer(
      //侧边栏按钮Drawer
      child: new ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
            new DrawerHeader(
                decoration: new BoxDecoration(
                    color: Colors.blue,
                ),
                child: new Container(
                    child: new Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                            //Material内置控件
                            new Text('你好 XXX', style: new TextStyle(color: Colors.white),),
                            new Text('18556732048', style: new TextStyle(color: Colors.white),),
                    ]),
                ) 
            ),
          new ListTile(
              //第一个功能项
              title: new Text('通用设置'),
              trailing: new Icon(Icons.arrow_right),
              onTap: () {
                Navigator.of(context).pop();
                //Navigator.of(context).push(new MaterialPageRoute(
                //    builder: (BuildContext context) => new SidebarPage()));
              }),
          new ListTile(
              //第二个功能项
              title: new Text('服务工单'),
              trailing: new Icon(Icons.arrow_right),
              onTap: () {
                Navigator.of(context).pop();
                // Navigator.of(context).push(new MaterialPageRoute(
                //     builder: (BuildContext context) => new SidebarPage()));
              }),
          new ListTile(
              //第二个功能项
              title: new Text('账号管理'),
              trailing: new Icon(Icons.arrow_right),
              onTap: () {
                Navigator.of(context).pop();
                // Navigator.of(context).pushNamed('/a');
              }),
          
          new ListTile(
            //退出按钮
            title: new Text('使用帮助'),
            trailing: new Icon(Icons.arrow_right),
            onTap: () => Navigator.of(context).pop(), //点击后收起侧边栏
          ),
          new ListTile(
            //退出按钮
            title: new Text('关于我们'),
            trailing: new Icon(Icons.arrow_right),
            onTap: () => Navigator.of(context).pop(), //点击后收起侧边栏
          ),
          new Divider(), //分割线控件
          new ListTile(
            //退出按钮
            title: new Text('version: 1.2.13(0.2.1)'),
            onTap: () => Navigator.of(context).pop(), //点击后收起侧边栏
          ),
        ],
      ),
    );
  }
}
