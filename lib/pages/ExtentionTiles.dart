import 'package:flutter/material.dart';
import '../pages/MeterList.dart';

class ExpansionTiles extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new ListView.builder(
          itemBuilder: (BuildContext context, int index) => new EntryItem(context, meterList[index]),
          itemCount: meterList.length,
        );
  }
}

// One entry in the multilevel list displayed by this app.
class Entry {
  Entry(String title, children){
      this.title = title;
      this.children = children;
      this.meterIds = [];
  }
  String title;
  List<int> meterIds;
  List<Entry> children;
}

// The entire multilevel list displayed by this app.
List<Entry> meterList = <Entry>[];

// Displays one Entry. If the entry has children then it's displayed
// with an ExpansionTile.
class EntryItem extends StatelessWidget {
    Entry entry;
    BuildContext context;
    EntryItem(BuildContext context, Entry entry){
        this.context = context;
        this.entry = entry;
    }

    Widget _buildTiles(Entry root) {
        var meterIdList = root.meterIds;
        var title = root.title;
        if (root.children.isEmpty){
            if(meterIdList.length > 0){
                return new FlatButton(
                    child: new ListTile(title: new Text(root.title)),
                    onPressed: () {
                        print("House cell tapped: $meterIdList");
                            Navigator.push(
                                this.context,
                                MaterialPageRoute(builder: (context) => AnimatedListSample(title, meterIdList)),  //new DetailPage(title, meterIdList),
                            );
                    }, 
                );
            }else{
                return new ListTile(title: new Text(root.title));
            }
        }
        return new ExpansionTile(
        key: new PageStorageKey<Entry>(root),
        title: new Text(root.title),
        children: root.children.map(_buildTiles).toList(),
        );
    }

    @override
    Widget build(BuildContext context) {
        this.context = context;
        return _buildTiles(entry);
    }
}