import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
import '../pages/SEMDrawer.dart';
import 'ExtentionTiles.dart';
import '../Utils.dart';

class SEMApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new SEMState();
  }
}

class SEMState extends State<SEMApp> {
    var _isLoading = true;
    var meters;
    genList0(Map record){
        int meterId = record["meterID"];
        var province = record["province"];
        var city = record["city"];
        var community = record["community"];
        var house = record["house"];
        int i = 0, j = 0, x = 0, y = 0;
        for(; i < meterList.length; i++){
            if(meterList[i].title == province){
                print("find province in list " + province);
                var cityList = meterList[i].children;
                for(; j < cityList.length; j++){
                    if(cityList[j].title == city){
                        print("find city in list " + city);
                        var communityList = cityList[j].children;
                        for(; x < communityList.length; x++){
                            if(communityList[x].title == community){
                                print("find comminity in list " + community);
                                var houseList = communityList[x].children;
                                for(; y < houseList.length; y++){
                                    if(houseList[y].title == house){
                                        print("find house in list " + houseList[y].title);
                                        var house = houseList[y];
                                        if(!house.meterIds.contains(meterId)){
                                            (((meterList[i].children)[j].children)[x].children)[y].meterIds.add(meterId);
                                        }
                                        return ;
                                    }
                                }
                                if(y == houseList.length){
                                    print("house not in list: $house, $meterId]]]");
                                    Entry entry = new Entry(house, <Entry>[]);
                                    entry.meterIds.add(meterId);
                                    (((meterList[i].children)[j].children)[x].children).add(entry);
                                    return ;
                                }
                            }
                        }
                        if(x == communityList.length){
                            print("community not in list: " + community);
                            Entry entry = new Entry(community, <Entry>[]);
                            ((meterList[i].children)[j].children).add(entry);
                            genList0(record);
                            return ;
                        }
                    }
                }
                if(x == cityList.length){
                    print("city not in list: " + city);
                    Entry entry = new Entry(city, <Entry>[]);
                    (meterList[i].children).add(entry);
                    genList0(record);
                    return ;
                }
            }
        }
        if(i == meterList.length){
            print("province not in list: " + province);
            Entry entry = new Entry(province, <Entry>[]);
            meterList.add(entry);
            genList0(record);
            return ;
        }
    }
    genList(List list){
        meterList = <Entry>[];
        list.forEach((record){
            genList0(record);
            print("==============================");
        });
    }

    dynamic _get(String url) async {
        final response = await http.get(url);

        if (response.statusCode == 200) {
            print(response.body);
            final map = json.decode(response.body);
            return map;
        } else {
            return Null;
        }
    }
    dynamic _post(String url, String body) async {
        final response = await http.post(url, body: body);
        if (response.statusCode == 200) {
            print(response.body);
            final map = json.decode(response.body);
            return map;
        } else {
            return Null;
        }
    }

    _fetchData() async {
        final url = "http://www.asuscn.space:3003/meters";
        print("Atempting to get data from " + url);
        final map = await _get(url);
        this.meters = map;
        genList(this.meters);
        setState(() {
            _isLoading = false;
        });
    }

    _postData(String mac, String province, String city, String community, String house) async {
        var url = "http://www.asuscn.space:3003/meter2house";
        // var body ='{"mac": "${mac}", "province": "${province}", "city": "${city}", "community": "${community}", "house": "${house}"}';
        var body ='mac=${mac}&province=${province}&city=${city}&community=${community}&house=${house}';
        print("_postData to " + url + " body: " + body);
        final response = await http.post(url, body: body, headers: {"Content-Type": "application/x-www-form-urlencoded"});
        if (response.statusCode == 200) {
            print(response.body);
            final map = json.decode(response.body);
            if(map["status"] == 0){
                //TODO: setState to refresh
            }
            return Null;
        } else {
            return Null;
        }
    }

  var loginState = false;
  @override
  void initState() {
    super.initState();
    setState(() {
      _isLoading = true;
    });
    _fetchData();
  }
    void _select(Choice choice) {
        switch (choice.title) {
            case '刷新列表':
                setState(() {
                    _isLoading = true;
                });
                _fetchData();
                break;
            case '新建电表':
                _createMeter();
                break;
            default:
                break;
        }
    }

    void _diagResult(DialogOptions value){
        Navigator.pop(context, value);
    }
    void _showAlert(String message){
        AlertDialog dialog = new AlertDialog(
            content: new Text(message, style: new TextStyle(fontSize: 20.0),),
            actions: <Widget>[
                new FlatButton(onPressed: (){_diagResult(DialogOptions.Cancel);}, child: new Text("确认"),),
            ],
        );
        showDialog(context: context, child: dialog);
    }
    Future<DialogOptions> _createMeter() async {
        String tempMac = "";
        String tempProvince = "";
        String tempCity = "";
        String tempCommunity = "";
        String tempHouse = "";
        switch (await showDialog<DialogOptions>(
            context: context,
            child: new SimpleDialog(
                    title: const Text('新建/修改电表位置'),
                    children: <Widget>[
                    new SimpleDialogOption(
                        child: new Row(
                            children: <Widget>[
                                const Text('Mac：', ),
                                new Expanded(
                                    child: new TextField(
                                        onChanged: (String value){tempMac = value;},
                                        decoration: new InputDecoration.collapsed(
                                        hintText: "如：江苏省",
                                        hintStyle: new TextStyle(color: Colors.grey)),
                                        ),
                                )
                            ],
                        )
                    ),
                    new SimpleDialogOption(
                        child: new Row(
                            children: <Widget>[
                                const Text('省份：'),
                                new Expanded(
                                    child: new TextField(
                                        onChanged: (String value){tempProvince = value;},
                                        decoration: new InputDecoration.collapsed(
                                        hintText: "如：江苏省",
                                        hintStyle: new TextStyle(color: Colors.grey)),),
                                )
                            ],
                        )
                    ),
                    new SimpleDialogOption(
                        child: new Row(
                            children: <Widget>[
                                const Text('城市：'),
                                new Expanded(
                                    child: new TextField(
                                        onChanged: (String value){tempCity = value;},
                                        decoration: new InputDecoration.collapsed(
                                        hintText: "如：苏州市",
                                        hintStyle: new TextStyle(color: Colors.grey)),
                                    ),
                                )
                            ],
                        )
                    ),
                    new SimpleDialogOption(
                        child: new Row(
                            children: <Widget>[
                                const Text('小区：'),
                                new Expanded(
                                    child: new TextField(
                                        onChanged: (String value){tempCommunity = value;},
                                        decoration: new InputDecoration.collapsed(
                                        hintText: "如：嘉业阳光假日",
                                        hintStyle: new TextStyle(color: Colors.grey)),),
                                )
                            ],
                        )
                    ),
                    new SimpleDialogOption(
                        child: new Row(
                            children: <Widget>[
                                const Text('户号：'),
                                new Expanded(
                                    child: new TextField(
                                        onChanged: (String value){tempHouse = value;},
                                        decoration: new InputDecoration.collapsed(
                                        hintText: "如：xx-xxx",
                                        hintStyle: new TextStyle(color: Colors.grey)),),
                                )
                            ],
                        )
                    ),
                    new SimpleDialogOption(
                        child: new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                                new FlatButton(onPressed: (){_diagResult(DialogOptions.OK);}, child: new Text("完成"),),
                                new FlatButton(onPressed: (){_diagResult(DialogOptions.Cancel);}, child: new Text("取消"),),
                            ],
                        )
                    ),
                    ],
                ),
        )) {
            case DialogOptions.OK:
                if(tempProvince == ""  || tempCity == "" ||
                    tempCommunity == "" || tempHouse == "" || 
                    tempMac == ""){
                        _showAlert("输入信息有误，请输入完整信息！");
                }else{
                    //_showAlert("输入信息正确");
                    //TODO: 将电表信息上传
                    _postData(tempMac, tempProvince, tempCity, tempCommunity, tempHouse);
                }
                break;
            case DialogOptions.Cancel:
                break;
        }
    }
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new Scaffold(
        appBar: new AppBar(
          title: _buildHeader(),
          actions: <Widget>[
            new PopupMenuButton<Choice>(
              onSelected: _select,
              itemBuilder: (BuildContext context) {
                return homePagePopMenu.map((Choice choice) {
                  return new PopupMenuItem<Choice>(
                    value: choice,
                    child: new Text(choice.title),
                  );
                }).toList();
              },
            ),
          ],
        ),
        drawer: new SEMDrawer(),
        body: new Center(
            child: _isLoading ? new CircularProgressIndicator():new ExpansionTiles(),
        ),
      ),
    );
  }

  Widget _buildHeader() {
    return new Row(
      children: <Widget>[
        new Expanded(
          child: new Container(
              margin: const EdgeInsets.only(top: 0.0, left: 0.0, bottom: 0.0),
              padding: const EdgeInsets.all(0.0),
              decoration: new BoxDecoration(
                borderRadius:
                    const BorderRadius.all(const Radius.circular(4.0)),
                color: Colors.white,
              ),
              child: new Row(
                children: <Widget>[
                  new Icon(Icons.search, color: Colors.grey,),
                  new Expanded(
                    child: new TextField(
                      //autofocus: true,
                      decoration: new InputDecoration.collapsed(
                          hintText: "房源名称/房源地址",
                          hintStyle: new TextStyle(color: Colors.grey)),
                    ),
                  ),
                  new IconButton(
						icon: new Icon(Icons.cancel, color: Colors.grey,),
						onPressed: (){
							print("search canceled");
						}
					)
                ],
              )),
        ),
      ],
    );
  }
}
