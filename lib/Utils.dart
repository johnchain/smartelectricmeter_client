import 'package:flutter/material.dart';

class MeterDetail{
    Map meterDetail;
    int meterId;
    int switchState;

    MeterDetail(int meterId){
        this.meterId = meterId;
        this.switchState = -1;
        this.meterDetail = {};
    }
}

class Choice {
    const Choice({ this.title, this.icon });
    final String title;
    final IconData icon;
}

const List<Choice> homePagePopMenu = const <Choice>[
    const Choice(title: '刷新列表', icon: Icons.directions_bike),
    const Choice(title: '新建电表', icon: Icons.directions_car),
];

const List<Choice> detailPagePopMenu = const <Choice>[
    const Choice(title: '刷新列表', icon: Icons.directions_bike),
];

enum DialogOptions { OK, NO, Cancel }

enum PostCMDs {
    INSERT,
    DELETE,
    UPDATE,
    SEARCH ,
    SWITCH,
    BIND,
    UNBIND
}